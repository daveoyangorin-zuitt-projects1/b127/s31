//Controllers contain the functions and business logic of our Express js application
//Meaning all the operation it can do will be placed in this file

//Allows us to use the contents of the "task.js" file in the models folder
const Task = require('../models/task');


//Controller function for getting all the tasks

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}


module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error)=> {
		if(error){
			console.log(error);
			return false;
		}else{
			return task;
		}
	})
}



//Delete task
//Business Logic
/*
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			//if an error is encountered returns a "false" boolean back to the client
			console.log(err)
			return false;
		}else{
			//Delete successful, returns the removed task object back to the client
			return removedTask;
		}
	})
}




//Updating a task
//Business Logic
/*
1. Get the task with the id using the method "findById"
2. Replace the task's name returned from the database with the "name" property form the request body
3. Save the task
*/

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(err)
			return false;
		}

		//Results of the "findById" method will be stored in the "result" parameter
		//It's "name" property will be reassigned the value of the "name" received form the request body
		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr)=>{
			if(saveErr){
				console.log(saveErr)
				return false;
			}else{
				//Update successfully, returns the updated task object back to the client
				return updatedTask;
			}
		})
	})
}

/*1*/
module.exports.getOneTask= (taskId) => {
	return Task.findById(taskId).then((result, error) => { 
		if(error) {
            console.log(error)
            return false;
        }

        return result;
	})
}

/*6*/module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(err)
			return false;
		}

		
		result.status = newContent.status;
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr)
				return false;
			} else {			
				return updatedTask;
			}
		})
	})
}