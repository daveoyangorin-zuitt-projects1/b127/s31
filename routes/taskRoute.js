
const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController');



router.get('/getAll', (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})


//Create new Task
router.post('/', (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})

//delete

router.delete('/:id', (req, res)=>{
	taskController.deleteTask(req.params.id).then(result => res.send(result));
})



//Update a task
router.put('/:id', (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})






module.exports = router;