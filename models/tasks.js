const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
	name:String,
	status:{
		type:String,
		default:'Pending'
	}
})

module.exports = mongoose.model("Task" , taskSchema);

//module.exports is a way for node js to treat his value as a  package that can be  used by ither files

//model -> controllers => routes => index.js